package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Patient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PatientRepositoryTest {

    @Autowired
    private PatientRepository repository;

    public static final String PATIENT_UUID = UUID.randomUUID().toString();

    @BeforeEach
    void setUp() {
        repository.save(Patient.builder()
                .name("Vitor Marques")
                .age(32)
                .cpf("111.328.227.70")
                .phone("21 969236537")
                .uuid(PATIENT_UUID)
                .build());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void shouldFindByUuid() {
        Patient patient = repository.findByUuid(PATIENT_UUID).get();
        assertAll(
                () -> assertNotNull(patient),
                () -> assertTrue(patient.getId() > 0),
                () -> assertTrue(PATIENT_UUID.equalsIgnoreCase(patient.getUuid())),
                () -> assertTrue(StringUtils.isNotBlank(patient.getName())),
                () -> assertTrue(StringUtils.isNotBlank(patient.getCpf())),
                () -> assertTrue(StringUtils.isNotBlank(patient.getPhone())),
                () -> assertTrue(StringUtils.isNotBlank(patient.getUuid())),
                () -> assertEquals(patient.getAge(), 32)
        );
    }
}