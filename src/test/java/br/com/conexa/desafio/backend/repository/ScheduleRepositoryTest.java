package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Schedule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ScheduleRepositoryTest {

    @Autowired
    private ScheduleRepository repository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @ParameterizedTest(name = "doctor {0}")
    @ValueSource(longs = {1001L, 9999L})
    void shouldFindDaySchedulesByDoctor(Long doctorId) {
        Page<Schedule> schedules = repository.findDaySchedulesByDoctor(doctorId, Pageable.unpaged());

        assertNotNull(schedules);
    }

    @ParameterizedTest(name = "doctor {0}")
    @ValueSource(longs = {1001L, 9999L})
    void shouldFindAllByDoctorId(Long doctorId) {
        Page<Schedule> schedules = repository.findAllByDoctorId(doctorId, Pageable.unpaged());

        assertNotNull(schedules);
    }
}