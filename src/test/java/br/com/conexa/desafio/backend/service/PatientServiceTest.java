package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.exception.NotFoundException;
import br.com.conexa.desafio.backend.exception.PatientAlreadyCreatedException;
import br.com.conexa.desafio.backend.model.Patient;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PatientServiceTest {

    @Autowired
    private PatientService service;

    public static final String PATIENT_UUID = UUID.randomUUID().toString();

    private Patient patient;

    @BeforeEach
    void setUp() {
        this.patient = Patient.builder()
                .name("Vitor Marques")
                .age(32)
                .cpf("111.328.227.70")
                .phone("21 969236537")
                .uuid(PATIENT_UUID)
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Order(1)
    void shouldFindAll() {
        assertDoesNotThrow(() -> service.findAll(Pageable.unpaged()).getTotalElements());
    }

    @Test
    @Order(2)
    void shouldSave() {
        assertNotNull(service.save(this.patient));
    }

    @Test
    @Order(3)
    void shouldThrowsPatientAlreadyCreatedException() {
        assertThrows(PatientAlreadyCreatedException.class,
                () -> service.save(this.patient));
    }

    @Test
    @Order(4)
    void shouldFindByUuid() {
        assertNotNull(service.findByUuid(PATIENT_UUID));
    }

    @Test
    @Order(5)
    void shouldThrowsNotFoundExceptionFindByUuid() {
        assertThrows(NotFoundException.class,
                () -> service.findByUuid(UUID.randomUUID().toString()));
    }

    @Test
    @Order(6)
    void shouldUpdate() {
        this.patient.setName("Vitor Marques Alterado");
        assertNotNull(service.update(this.patient));
    }

    @Test
    @Order(7)
    void shouldThrowsNotFoundExceptionUpdate() {
        assertThrows(NotFoundException.class,
                () -> service.update(new Patient()));
    }

    @Test
    @Order(8)
    void shouldDelete() {
        assertDoesNotThrow(() -> service.delete(PATIENT_UUID));
    }

    @Test
    @Order(9)
    void shouldThrowsNotFoundExceptionDelete() {
        assertThrows(NotFoundException.class,
                () -> service.delete(UUID.randomUUID().toString()));
    }
}