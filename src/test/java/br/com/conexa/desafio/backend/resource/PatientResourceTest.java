package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.dto.request.PatientRequest;
import br.com.conexa.desafio.backend.model.Patient;
import br.com.conexa.desafio.backend.service.PatientService;
import br.com.conexa.desafio.backend.service.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PatientResourceTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PatientService patientService;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Order(4)
    void shouldGetAll() throws Exception {

        mockMvc.perform(
                get("/patients")
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    @Order(3)
    void shouldGetByUuid() throws Exception {

        String patientUuid = patientService.findAll(Pageable.unpaged()).stream().findFirst().get().getUuid();

        mockMvc.perform(
                get("/patients/"+patientUuid)
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());

    }

    @Test
    @Order(1)
    void shouldSaveWithoutCredentials() throws Exception {

        PatientRequest patientRequest = new PatientRequest();
        patientRequest.setName("Patient Vitor Marques crazy");
        patientRequest.setAge(55);
        patientRequest.setCpf("111.328.217-70");
        patientRequest.setPhone("21 965624575");

        mockMvc.perform(
                post("/patients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(patientRequest))
        ).andExpect(
                status().isCreated())
        .andExpect(
                header().exists(HttpHeaders.LOCATION)
        );
    }

    @Test
    @Order(2)
    void shouldUpdate() throws Exception {

        Patient patient = patientService.findAll(Pageable.unpaged()).stream().findFirst().get();
        patient.setName("Some Patient updated");

        PatientRequest patientRequest = new PatientRequest();
        patientRequest.setPhone(patient.getPhone());
        patientRequest.setCpf(patient.getCpf());
        patientRequest.setAge(patient.getAge());
        patientRequest.setName(patient.getName());

        mockMvc.perform(
                put("/patients/"+patient.getUuid())
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(patientRequest))
        ).andExpect(status().isOk());
    }

    @Test
    @Order(5)
    void shouldDelete() throws Exception {

        String patientUuid = patientService.findAll(Pageable.unpaged()).stream().findFirst().get().getUuid();

        mockMvc.perform(
                delete("/patients/"+patientUuid)
                        .header(
                                HttpHeaders.AUTHORIZATION,
                                "Bearer " + tokenService.generateToken("usernamedomedico@email.com" + Math.random()))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNoContent());

    }
}