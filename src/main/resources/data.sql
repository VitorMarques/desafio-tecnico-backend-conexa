--Specialty
INSERT INTO conexa.specialty (id, uuid, description) values(1, UUID(), 'cardiology');
INSERT INTO conexa.specialty (id, uuid, description) values(2, UUID(), 'oncology');
INSERT INTO conexa.specialty (id, uuid, description) values(3, UUID(), 'dermatology');
INSERT INTO conexa.specialty (id, uuid, description) values(4, UUID(), 'epidemiology');
INSERT INTO conexa.specialty (id, uuid, description) values(5, UUID(), 'ophthalmology');

--Doctor
INSERT INTO conexa.doctor (id, name, password, username, uuid, specialty_id, account_non_expired, account_non_locked, credentials_non_expired, enabled)
    VALUES (1001, 'Dr. Daniel Santos', '$2a$04$XQT97KqierRh4qekKgX4SuZKUP/q9B9z0ydEsPs8r3tvw9r/B.hPW', 'medico@email.com', UUID(), 1, true, true, true, true); --password=senhadomedico

INSERT INTO conexa.doctor (id, name, password, username, uuid, specialty_id, account_non_expired, account_non_locked, credentials_non_expired, enabled)
    VALUES (1002, 'Dr. Everton Ribeiro', '$2a$04$zV9UtKljcb9FKruPnNolb.4HTvgnbf2ultSQE0s6NxjXJvw6RCgju', 'medico1@email.com', UUID(), 2, true, true, true, true); --password=senhadomedico1

INSERT INTO conexa.doctor (id, name, password, username, uuid, specialty_id, account_non_expired, account_non_locked, credentials_non_expired, enabled)
    VALUES (1003, 'Dr. Bruno Henrique', '$2a$04$tzK/wmsAFvC/2j2T47UhOeVgs5nHm6A7WRZ3yZ2llJXcA9HTRIp/S', 'medico2@email.com', UUID(), 3, true, true, true, true); --password=senhadomedico2

INSERT INTO conexa.doctor (id, name, password, username, uuid, specialty_id, account_non_expired, account_non_locked, credentials_non_expired, enabled)
    VALUES (1004, 'Dr. Vitor Marques', '$2a$04$JrjEjpeCBRixfXiR0KWV8.4tNvnvTiY8m8aHVOe6bgFf7ct8Rc18q', 'medico3@email.com', UUID(), 4, true, true, true, true); --password=senhadomedico3

INSERT INTO conexa.doctor (id, name, password, username, uuid, specialty_id, account_non_expired, account_non_locked, credentials_non_expired, enabled)
    VALUES (1005, 'Dr. Pedro Neves', '$2a$04$pswCQeRlaRCyk2oZ/hOjRu238jec/Yuv/y0ag4Y9GR7n7aioNFn8u', 'medico4@email.com', UUID(), 5, true, true, true, true); --password=senhadomedico4

--Patient
INSERT INTO conexa.patient (id, age, cpf, name, phone, uuid)
    VALUES(1000, 32, '111.328.217-70', 'Alguem Doente', '21 97765-9876', UUID());

--Schedule
INSERT INTO conexa.schedule(id, service_date_time, uuid, doctor_id, patient_id)
    VALUES(1000, DATE_ADD(NOW(), INTERVAL 7 DAY), UUID(), 1004, 1000);

INSERT INTO conexa.schedule(id, service_date_time, uuid, doctor_id, patient_id)
    VALUES(1002, NOW(), UUID(), 1001, 1000);

INSERT INTO conexa.schedule(id, service_date_time, uuid, doctor_id, patient_id)
    VALUES(1003, DATE_ADD(NOW(), INTERVAL 7 DAY), UUID(), 1001, 1000);