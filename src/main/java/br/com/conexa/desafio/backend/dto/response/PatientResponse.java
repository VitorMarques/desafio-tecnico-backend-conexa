package br.com.conexa.desafio.backend.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientResponse {

    private String uuid;
    private String name;
    private String cpf;
    private int age;
    private String phone;

}
