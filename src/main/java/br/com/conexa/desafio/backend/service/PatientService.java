package br.com.conexa.desafio.backend.service;

import br.com.conexa.desafio.backend.exception.NotFoundException;
import br.com.conexa.desafio.backend.exception.PatientAlreadyCreatedException;
import br.com.conexa.desafio.backend.model.Patient;
import br.com.conexa.desafio.backend.repository.PatientRepository;
import br.com.conexa.desafio.backend.util.MessageUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PatientService {

    private final PatientRepository patientRepository;
    private final MessageUtil messageUtil;

    @Cacheable("patients")
    public Page<Patient> findAll(Pageable pageable) {
        return patientRepository.findAll(pageable);
    }

    public Patient findByUuid(String uuid) throws NotFoundException {
        return patientRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException(messageUtil.getMessage("error.patient.id.not.found", uuid)));
    }

    @Transactional(readOnly = false)
    @CacheEvict(value = "patients", allEntries = true)
    public void delete(String uuid) throws NotFoundException {

        Patient patient = this.findByUuid(uuid);

        log.info("Removing patient " + patient.getName());

        patientRepository.delete(patient);
    }

    @Transactional(readOnly = false)
    @CacheEvict(value = "patients", allEntries = true)
    public Patient save(Patient patient) throws PatientAlreadyCreatedException {

        log.info("Inserting a new patient on the system: " + patient.getName());

        Optional<Patient> optionalPatient = patientRepository.findByUuid(patient.getUuid());
        if(optionalPatient.isPresent())
            throw new PatientAlreadyCreatedException(messageUtil.getMessage("error.patient.already.created", patient.getUuid()));

        return patientRepository.save(patient);
    }

    @Transactional(readOnly = false)
    @CacheEvict(value = "patients", allEntries = true)
    public Patient update(Patient patient) throws NotFoundException {

        log.info("Updating patient " + patient.getName() + ". ID=" + patient.getId());

        Patient oldPatient = this.findByUuid(patient.getUuid());

        oldPatient.setName(patient.getName());
        oldPatient.setAge(patient.getAge());
        oldPatient.setCpf(patient.getCpf());
        oldPatient.setPhone(patient.getPhone());

        return patientRepository.save(oldPatient);
    }
}
