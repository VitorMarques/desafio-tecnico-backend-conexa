package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Token;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends PagingAndSortingRepository<Token, Long> {

    Optional<Token> findByUsername(String username);
}
