package br.com.conexa.desafio.backend.resource;

import br.com.conexa.desafio.backend.converter.response.LoginResponseConverter;
import br.com.conexa.desafio.backend.dto.request.LoginRequest;
import br.com.conexa.desafio.backend.dto.response.LoginResponse;
import br.com.conexa.desafio.backend.model.Doctor;
import br.com.conexa.desafio.backend.service.TokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/authentication")
public class AuthenticationResource extends Resource {

    private final TokenService tokenService;
    private final AuthenticationManager authenticationManager;
    private final LoginResponseConverter loginResponseConverter;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody @Valid LoginRequest loginRequest) {

        log.info("User " + loginRequest.getUsername() + " trying to login on the system...");

        UsernamePasswordAuthenticationToken credentials =
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword(), new ArrayList<>());

        Authentication authentication = authenticationManager.authenticate(credentials);

        log.info("The Credentials are valid...");

        Doctor doctor = (Doctor) authentication.getPrincipal();

        ResponseEntity<LoginResponse> loginResponse = ok(doctor, loginResponseConverter);

        Objects.requireNonNull(loginResponse.getBody()).setToken(tokenService.generateToken(doctor.getUsername()));

        log.info("Returning token and user information: [" + loginResponse.toString() + "]");

        return loginResponse;
    }

    @GetMapping("/logout")
    public ResponseEntity<?> logout(@RequestHeader(name = "Authorization") String token) {

        log.info("Invalidating token " + token);

        tokenService.invalidate(token.replace("Bearer ", ""));

        return noContent();
    }

}
