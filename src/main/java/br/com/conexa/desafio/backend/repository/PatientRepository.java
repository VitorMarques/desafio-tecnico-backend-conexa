package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Patient;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends PagingAndSortingRepository<Patient, Long> {

    Optional<Patient> findByUuid(String uuid);

}
