package br.com.conexa.desafio.backend.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "doctor")
public class Doctor implements Model, UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true, updatable = false)
    private String uuid;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 100, nullable = false, unique = true)
    private String username;

    @Column(length = 120, nullable = false, unique = true)
    private String password;

    @Transient
    private Collection<? extends GrantedAuthority> authorities;

    private boolean accountNonExpired = Boolean.TRUE;

    private boolean accountNonLocked = Boolean.TRUE;

    private boolean credentialsNonExpired = Boolean.TRUE;

    private boolean enabled = Boolean.TRUE;

    @ManyToOne(fetch = FetchType.LAZY)
    private Specialty specialty;

    @OneToMany(mappedBy = "doctor", fetch = FetchType.LAZY)
    private Set<Schedule> schedules;

    @Override
    public String toString() {
        return "Doctor{" +
                "uuid='" + uuid + '\'' +
                '}';
    }
}
