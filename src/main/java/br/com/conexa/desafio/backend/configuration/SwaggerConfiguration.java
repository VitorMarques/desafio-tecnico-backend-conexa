package br.com.conexa.desafio.backend.configuration;

import br.com.conexa.desafio.backend.resource.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
@ComponentScan(basePackageClasses = {Resource.class})
public class SwaggerConfiguration {

    public static final Contact DEFAULT_CONTACT
            = new Contact(
            "Vitor Marques",
            "http://localhost:8080/conexa-api",
            "vitormarques.sa@gmail.com");

    public static final ApiInfo DEFAULT_API_INFO
            = new ApiInfo(
            "Conexa Saude Api Documentation",
            "Api Documentation",
            "1.0",
            "urn:tos",
            DEFAULT_CONTACT,
            "Apache 2.0",
            "http://www.apache.org/licenses/LICENSE-2.0",
            new ArrayList<>());

    public static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<>(Arrays.asList("application/json", "application/xml"));

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
                .pathMapping("/conexa-api")
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(
                    PathSelectors.ant("/conexa-api/patients/**")
                        .or(PathSelectors.ant("/conexa-api/doctors/**"))
                        .or(PathSelectors.ant("/conexa-api/authentication/**"))
                )
                .build()
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class);
    }

}

