package br.com.conexa.desafio.backend.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {

    @NotNull(message = "{login.username.not.null}")
    @NotEmpty(message = "{login.username.not.empty}")
    private String username;

    @NotNull(message = "{login.password.not.null}")
    @NotEmpty(message = "{login.password.not.empty}")
    private String password;

}
