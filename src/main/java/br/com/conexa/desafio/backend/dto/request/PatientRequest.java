package br.com.conexa.desafio.backend.dto.request;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class PatientRequest {

    private String uuid;

    @NotNull(message = "{patient.name.not.null}")
    @NotEmpty(message = "{patient.name.not.empty}")
    private String name;

    @NotNull(message = "{patient.cpf.not.null}")
    @NotEmpty(message = "{patient.cpf.not.empty}")
    @CPF(message = "{patient.cpf.invalid}")
    private String cpf;

    @Positive(message = "{patient.age.invalid}")
    private int age;

    @NotNull(message = "{patient.phone.not.null}")
    @NotEmpty(message = "{patient.phone.not.empty}")
    private String phone;

}
