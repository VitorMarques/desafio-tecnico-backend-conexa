package br.com.conexa.desafio.backend.repository;

import br.com.conexa.desafio.backend.model.Doctor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DoctorRepository extends PagingAndSortingRepository<Doctor, Long> {

    Optional<Doctor> findByUuid(String uuid);

}
