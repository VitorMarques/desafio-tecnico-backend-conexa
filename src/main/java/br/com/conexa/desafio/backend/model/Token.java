package br.com.conexa.desafio.backend.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "token")
public class Token implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true, updatable = false)
    private String token;

    @Column(nullable = false)
    private String username;

    @Column(name = "expiration_dt", nullable = false)
    private LocalDate expirationDate;

    @Column(nullable = false)
    private boolean valid = Boolean.TRUE;

}
