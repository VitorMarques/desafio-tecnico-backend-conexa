# Solucao Vitor Marques
<!-- TOC -->

- [ConexaSaudeApi](#solucao-vitor-marques)
    - [Arquitetura](#arquitetura)
    - [Documentação](#documentacao)
    - [Montagem do Ambiente](#montagem-do-ambiente)
    - [Executando os testes](#executando-os-testes)

## Arquitetura

<!-- TOC -->

- A aplicação ConxaSaudeApi foi totalmente desenvolvida em Java 8 utilizando as facilidades do framework SpringBoot.

- Para suportar as necessidades de persistência de dados foi utilizado o banco MySQL instalado localmente em um container Docker.

- Para tratar a autenticação e autorização dos endpoints da API foi utilizado o SpringSecurity em conjunto com a API JWT para geração de Tokens.

- Visando otimizar a performance e utilização de recursos foi configurado um mecanismo de cache utilizando a implementação EhCahce.

- Como parte do pacote de infra estrutura foi implementado um mecanismo de log eficiente em cada requisição efetuada para a API utilizando o provider logback.

- A fim de diminuir a verbosidade e melhorar a legibilidade do código foi utilizada a biblioteca lombok que provê a criação de getters, setters, constructors e outras facilidades de forma implícita.

- Foi implementado Content-Negotiation para que as respostas da API possam ser nos formatos JSON ou XML, de acordo com a necessidade do Client. Bastanto esse informar o Header "Accept" na requisição.

- Ao iniciar a aplicação é executado o arquivo data.sql que é responsável por inserir os dados iniciais dos médicos, pacientes e agendamentos no sistema. Dentro deste arquivo estão as senhas de cada médico previamente cadastrado.

- Todas as exceções lançadas pela aplicação são tratadas por um Handler customizado que devolve para o client um json com as inforamções do erro em um formato de JSON.

## Documentacao
Com o intuito de permitir que qualquer client possa consumir a API sem dificuldades, foi criada uma documentação através da API SWAGGER-SPRINGFOX.
Para acessar a documentação é necessário que a aplicação esteja sendo executada.

Os endpoints da documentação são:
- Documentação Visual:
    http://${host}:${port}/conexa-api/swagger-ui/index.html#/

- Documentação em JSON:
    http://${host}:${port}/conexa-api/v2/api-docs

## Montagem do Ambiente  

#### Pré-Requisitos

- Java 8 instalado na máquina;
- Variável de ambiente JAVA_HOME configurada e apontando para o JDK do Java 8 ou superior;
- Git instalado na máquina ;
- Base de Dados MySQL rodando localmente;
- Alteração do arquivo application.properties para incluir a URL, usuário e senha da instância local do MySQL 

### Código-Fonte

**clonar a aplicação** `git clone https://gitlab.com/conexasaude-public/desafio-tecnico-backend-conexa.git`

Uma vez que o código da aplicação disponível rodar os seguintes comandos:

**rodar a aplicação no linux:** `cd desafio-tecnico-backend-conexa && mvnw spring-boot:run`
**rodar a aplicação no winidows:** `cd desafio-tecnico-backend-conexa && .\mvnw.cmd spring-boot:run`

## Executando os testes

**rodar os testes no linux:** `cd desafio-tecnico-backend-conexa && mvnw test`
**rodar os testes no windows:** `cd desafio-tecnico-backend-conexa && .\mvnw.cmd test`  




